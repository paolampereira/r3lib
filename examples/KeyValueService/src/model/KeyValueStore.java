/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;

/**
 *
 * @author paola
 */
public class KeyValueStore {

    private HashMap<Integer, Person> people;
    private static KeyValueStore instance;

    public static KeyValueStore getInstance() {
        if (instance == null) {
            instance = new KeyValueStore();
        }
        return instance;
    }

    private KeyValueStore() {
        this.people = new HashMap<Integer, Person>();
    }

    public void addPerson(Integer key, Person person) {
        this.people.put(key, person);
    }

    public void removePerson(Integer key) {
        this.people.remove(key);
    }

    public Person getPerson(Integer key) {
        return this.people.get(key);
    }
}
