/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package keyvalueclient;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Person;
import r3lib.communication.R3LibProxy;
import r3lib.communication.Response;

/**
 *
 * @author paola
 */
public class KeyValueClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int numThreads;
        int initialClientId;
        int sleep_inicial = 0;
        int sleep = 0;
        String jar0 = "", jar1 = "";
        String appName = "kv0";

        /*if (args.length == 6) {
            sleep_inicial = Integer.parseInt(args[2]);
            sleep = Integer.parseInt(args[3]);
            jar0 = args[4];
            jar1 = args[5];
            appName += "0";
        } else if (args.length == 3) {
            appName += (args[2]);
        } else {
            appName += "0";
        }*/
        System.out.println(appName);

        initialClientId = Integer.parseInt(args[0]);
        numThreads = Integer.parseInt(args[1]);

        // lança thread que inscreve libs e instancia apps
        if (initialClientId == -1 && args.length == 6) {
            try {
                // MUDAR PARA EXPERIMENTO DE INSCRIÇÃO
                System.out.println("creator thread");

                initialClientId++;
                Thread t = new Thread(new CreatorThread(sleep_inicial, sleep, jar0, jar1));
                t.start();
            } catch (IOException ex) {
                Logger.getLogger(KeyValueClient.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(KeyValueClient.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {

            Thread[] threads = new Thread[numThreads];
            for (int i = 0; i < numThreads; i++) {
                // creating a client id for each client
                int clientId = initialClientId + i;

                try {
                    threads[i] = new Thread(new ClientThread(clientId, appName));
                } catch (IOException ex) {
                    Logger.getLogger(KeyValueClient.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(KeyValueClient.class.getName()).log(Level.SEVERE, null, ex);
                }
                threads[i].start();
            }
        }

    }

}

class CreatorThread implements Runnable {

    private final R3LibProxy r3lib;
    private int sleep, sleep_inicial; // em segundos
    private String jar0, jar1;

    public CreatorThread(int sleep_inicial, int sleep, String jar0, String jar1) throws IOException, InterruptedException {
        System.out.println("creator thread");
        r3lib = new R3LibProxy(3);
        this.sleep = sleep;
        this.sleep_inicial = sleep_inicial;
        this.jar0 = jar0;
        this.jar1 = jar1;
    }

    @Override
    public void run() {

        try {

            Thread.sleep(150000);
            while (true) {
                //this.r3lib.newLib(Paths.get("/home/paola/Documents/tcc/genericrsm/KeyValueService/dist/KeyValueService.jar"), "keyvalueservice" + i);
                //this.r3lib.newLib(Paths.get("/home/KeyValueService.jar"), "keyvalueservice" + i);
                long startTime = System.nanoTime();
                this.r3lib.newLib(Paths.get("/local/genericrsm/" + jar0), "jpaxos"); //jpaxos.jar
                long endTime = System.nanoTime();
                long duration = (endTime - startTime);

                System.out.println(duration);
                Thread.sleep(100000); // 30 segundos

                startTime = System.nanoTime();
                this.r3lib.newLib(Paths.get("/local/genericrsm/" + jar1), "junit"); //junit.jar
                endTime = System.nanoTime();
                duration = (endTime - startTime);

                System.out.println(duration);
                Thread.sleep(100000); // 30 segundos

                startTime = System.nanoTime();
                this.r3lib.newLib(Paths.get("/local/genericrsm/commons-math-2.2.jar"), "commons"); //junit.jar
                endTime = System.nanoTime();
                duration = (endTime - startTime);

                System.out.println(duration);
                //this.r3lib.newAppInstance("keyvalueservice", "kv" + i);
                Thread.sleep(100000); // 30 segundos

                startTime = System.nanoTime();
                this.r3lib.newLib(Paths.get("/local/genericrsm/netty-all-4.1.9.Final.jar"), "netty"); //junit.jar
                endTime = System.nanoTime();
                duration = (endTime - startTime);

                System.out.println(duration);
                Thread.sleep(100000); // 30 segundos

            }

        } catch (IOException ex) {
            Logger.getLogger(CreatorThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(CreatorThread.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}

class ClientThread implements Runnable {

    private final int maxKey = 1000000;
    private final int clientId;
    private final R3LibProxy r3lib;
    private final String appName;

    public ClientThread(int clientId, String appName) throws IOException, InterruptedException {
        this.clientId = clientId;
        this.r3lib = new R3LibProxy(this.clientId);
        this.appName = appName;

    }

    @Override
    public void run() {

        Person p = new Person("name", "addr", "mail@mail.com");
        int i = 0;
        int key = 0;
        long startTime = 0;

        try {

            /*this.r3lib.newAppInstance("keyvalueservice", "abc");
            this.r3lib.invokeMethod("kv.controller.KeyValueController.add", key, p);
            
            Response r = this.r3lib.invokeMethod("kv.controller.KeyValueController.get", key);
            Person p1 = (Person) r.returnObject;
            System.out.println("primeiro retorno " + p1.getName());
            
            Response r2 = this.r3lib.invokeMethod("abc.controller.KeyValueController.get", key);
            Person p2 = (Person) r2.returnObject;
            System.out.println("segundo retorno " + p2.getName());
             */
            while (true) {

                if (this.getRandomInt() % 2 == 0) {

                    key = this.getRandomInt();
                    p.setName("name " + i);

                    startTime = System.nanoTime();
                    this.r3lib.invokeMethod(appName + ".controller.KeyValueController.add", key, p);

                } else {
                    key = this.getRandomInt();
                    startTime = System.nanoTime();
                    Response r = this.r3lib.invokeMethod(appName + ".controller.KeyValueController.get", key);
                    //Person p2 = (Person) r.returnObject;
                }

                if (i % 50 == 0) {
                    long endTime = System.nanoTime();
                    long duration = (endTime - startTime);

                    System.out.println(duration);
                }

                i++;
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Erro na execução do programa");
        }
    }

    private String getRandomString(int size) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVXZ";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < size) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    private int getRandomMethod() {
        Random rnd = new Random();
        return (Integer) rnd.nextInt(2);
    }

    private Integer getRandomInt() {
        Random rnd = new Random();
        return (Integer) rnd.nextInt(maxKey);
    }

}
