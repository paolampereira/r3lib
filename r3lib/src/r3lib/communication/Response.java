/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r3lib.communication;

import java.io.Serializable;

/**
 *
 * @author paola
 */
public class Response implements Serializable {

    public Object returnObject;
    public String errorMessage;
    public boolean success;

    public Response(Object returnObject, boolean success, String errorMessage) {
        this.returnObject = returnObject;
        this.success = success;
        this.errorMessage = errorMessage;
    }
}
