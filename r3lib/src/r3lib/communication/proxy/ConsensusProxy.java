/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r3lib.communication.proxy;

import r3lib.communication.Command;
import r3lib.communication.Response;

/**
 *
 * @author paola
 */
public abstract class ConsensusProxy {

    private final int paxosClientId;
    
    public ConsensusProxy(int id) {
        this.paxosClientId = id;
    }

    public abstract Response send(String namespace, Command cmd);

}
