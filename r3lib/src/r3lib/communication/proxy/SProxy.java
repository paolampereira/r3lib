/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r3lib.communication.proxy;

import bftsmart.tom.ServiceProxy;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import lsr.paxos.ReplicationException;
import lsr.paxos.client.Client;
import r3lib.communication.Command;
import r3lib.communication.Response;

/**
 *
 * @author paola
 */
public class SProxy extends ConsensusProxy {
    
    private final Client sClient;
    
    public SProxy(int paxosClientId) throws IOException {
        super(paxosClientId);
        sClient = new Client();
        sClient.connect();
    }

    @Override
    public Response send(String namespace, Command cmd) {
        
        Response r = new Response(null, false, "Error");

        try {
            // enviando requisição
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            
            ObjectOutputStream oos = new ObjectOutputStream(out);
            oos.writeUTF(namespace);
            oos.writeObject(cmd);

            byte[] reply = sClient.execute(out.toByteArray());

            ByteArrayInputStream in = new ByteArrayInputStream(reply);
            ObjectInputStream is = new ObjectInputStream(in);
            r = (Response) is.readObject();

        } catch (Exception e) {
            Logger.getLogger(BFTProxy.class.getName()).log(Level.SEVERE, null, e);
        } 
        
        return r;
    }
}
