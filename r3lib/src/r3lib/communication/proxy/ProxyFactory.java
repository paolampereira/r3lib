/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r3lib.communication.proxy;

import java.io.IOException;

/**
 *
 * @author paola
 */
public final class ProxyFactory {

    public static ConsensusProxy createProxy(String protocol, int clientId) throws IOException, InterruptedException {
        switch(protocol){
            case "bftsmart":
                return new BFTProxy(clientId);
            case "spaxos":
                return new SProxy(clientId);
        }
        return null;
    }
}
